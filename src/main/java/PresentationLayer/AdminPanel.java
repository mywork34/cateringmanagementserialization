package PresentationLayer;
import BusinessLayer.BaseProduct;
import BusinessLayer.DeliveryService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AdminPanel extends JFrame {
    private JPanel adminPanel;
    private JPanel adminFunctions;
    private JPanel indexPanel;
    private JLabel indexLabel = new JLabel("Insert index for REMOVE/MODIFY:");
    private JTextField removeIdText;
    private JButton csvButton = new JButton("ImportCSV");
    private JButton addButton = new JButton("AddItem");
    private JButton removeButton = new JButton("RemoveItem");
    private JButton modifyButton = new JButton("ModifyItem");
    private JButton addCompositeButton = new JButton("AddComposite");

    public AdminPanel()
    {
        setTitle("ADMIN");
        adminFunctions = new JPanel(new GridLayout(6,1,1,1));
        setDefaultCloseOperation(javax.swing.
                WindowConstants.DISPOSE_ON_CLOSE);
        removeIdText = new JTextField();
        indexPanel = new JPanel(new GridLayout(1,2,1,1));
        indexPanel.add(indexLabel);
        indexPanel.add(removeIdText);

        adminFunctions.add(csvButton);
        adminFunctions.add(addButton);
        adminFunctions.add(removeButton);
        adminFunctions.add(modifyButton);
        adminFunctions.add(addCompositeButton);

        adminPanel = new JPanel();

        adminPanel.add(adminFunctions);
        adminPanel.add(indexPanel);

        add(adminPanel);

        csvButton.addActionListener(new csvActionListener());
        addButton.addActionListener(new addActionListener());
        removeButton.addActionListener(new removeActionListener());
        modifyButton.addActionListener(new modifyActionListener());
        addCompositeButton.addActionListener(new addCompositeActionListener());
        setSize(420, 250);
    }

    class csvActionListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            DeliveryService deliveryService = new DeliveryService();
            deliveryService.getProducts();
            deliveryService.listMenuItems();
        }
    }

    class addActionListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            BaseProductInsert bi = new BaseProductInsert();
        }
    }

    class removeActionListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            DeliveryService deliveryService = new DeliveryService();
            int index = Integer.parseInt(removeIdText.getText());
            deliveryService.removeItem(index);
            JOptionPane.showMessageDialog(adminFunctions,"The item with index " + index + " was removed from the list!");
        }
    }

    class modifyActionListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            ModifyProductPanel bi = new ModifyProductPanel();

        }
    }

    class addCompositeActionListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            CompositeProductInsert ci = new CompositeProductInsert();
        }
    }
}
