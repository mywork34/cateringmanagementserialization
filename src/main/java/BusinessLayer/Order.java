package BusinessLayer;

import java.io.Serializable;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.util.Objects;

public class Order implements Serializable {
    private static int orderCount;
    private int orderID;
    private int clientID;
    private LocalDateTime orderDate;
    private float price;

    public Order(int clientID, LocalDateTime orderDate,float price){
        orderCount++;
        this.orderID = orderCount;
        this.clientID = clientID;
        this.orderDate = orderDate;
        this.price = price;
        System.out.println("Order["+orderID+"] with price["+price+"] was added by client["+clientID+"] at date -- "+orderDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderID, clientID, orderDate);
    }

    @Override
    public boolean equals(Object obj) {
        if(this==obj)
            return true;
        if(obj == null || obj.getClass()!= this.getClass())
            return false;
        Order order = (Order)obj;
        return (orderID==order.orderID && clientID==order.clientID && (orderDate.compareTo(order.orderDate)==0 ? true:false));
    }

    public int getOrderId(){
        return orderID;
    }

    public static void setOrderCount(int ordC){orderCount = ordC;}

    public int getClientId(){
        return clientID;
    }

    public float getPrice(){
        return price;
    }

    public int getHour(){
        return orderDate.getHour();
    }

    public DayOfWeek getDay(){
        return orderDate.getDayOfWeek();
    }

}
