package Models;

import DataLayer.AccountValidator;

import java.io.Serializable;

public class Client implements Serializable {
    private static int contID = 0;
    private int clientID;
    private String username;
    private String password;

    public Client(String username,String password){
        contID++;
        clientID = contID;
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public int getClientId(){
        return clientID;
    }
}
