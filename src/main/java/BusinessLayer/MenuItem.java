package BusinessLayer;

import java.io.Serializable;

public abstract class MenuItem implements Serializable {
    public abstract String getName();

    public abstract float getRating();

    public abstract int getNoCal();

    public abstract int getProteins();

    public abstract int getFats();

    public abstract int getSodium();

    public abstract float getPrice();

    public abstract void setName(String name);

    public abstract void setRating(float rating);

    public abstract void setNoCal(int noCal);

    public abstract void setProteins(int proteins);

    public abstract void setFats(int fats);

    public abstract void setSodium(int sodium);

    public abstract void setPrice(float price);
}
