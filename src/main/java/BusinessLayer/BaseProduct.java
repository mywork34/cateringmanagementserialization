package BusinessLayer;

public class BaseProduct extends MenuItem{
    private String name;
    private float rating;
    private int noCal;
    private int proteins;
    private int fats;
    private int sodium;
    private float price;

    public BaseProduct(String name,float rating,int noCal,int proteins,int fats,int sodium,float price){
        this.name = name;
        this.rating = rating;
        this.noCal = noCal;
        this.proteins = proteins;
        this.fats = fats;
        this.sodium = sodium;
        this.price = price;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public float getRating() {
        return rating;
    }

    @Override
    public int getNoCal() {
        return noCal;
    }

    @Override
    public int getProteins() {
        return proteins;
    }

    @Override
    public int getFats() {
        return fats;
    }

    @Override
    public int getSodium() {
        return sodium;
    }

    @Override
    public float getPrice() {
        return price;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public void setNoCal(int noCal) {
        this.noCal =noCal;
    }

    @Override
    public void setProteins(int proteins) {
        this.proteins = proteins;
    }

    @Override
    public void setFats(int fats) {
        this.fats = fats;
    }

    @Override
    public void setSodium(int sodium) {
        this.sodium = sodium;
    }

    @Override
    public void setPrice(float price) {
        this.price = price;
    }
}
