package PresentationLayer;

import BusinessLayer.BaseProduct;
import BusinessLayer.DeliveryService;
import BusinessLayer.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.HashSet;

public class CompositeProductInsert extends JFrame{
    private JPanel compositePanel;
    private JPanel productsPanel;
    private JLabel prod1L = new JLabel("Product1:");
    private JLabel prod2L = new JLabel("Product2:");;
    private JLabel prod3L = new JLabel("Product3:");;
    private JLabel prod4L = new JLabel("Product4:");;
    private JComboBox prod1;
    private JComboBox<String> prod2;
    private JComboBox<String> prod3;
    private JComboBox<String> prod4;

    public CompositeProductInsert(){
        compositePanel = new JPanel();
        productsPanel = new JPanel(new GridLayout(4,2,1,1));
        DeliveryService deliveryService = new DeliveryService();
        String[] menuList = getMenuList(deliveryService.getMenuItems());
        prod1 = new JComboBox(menuList);
        prod2 = new JComboBox(menuList);
        prod3 = new JComboBox(menuList);
        prod4 = new JComboBox(menuList);

        productsPanel.add(prod1L);
        productsPanel.add(prod1);
        productsPanel.add(prod2L);
        productsPanel.add(prod2);
        productsPanel.add(prod3L);
        productsPanel.add(prod3);
        productsPanel.add(prod4L);
        productsPanel.add(prod4);

        add(productsPanel);
        setSize(300,200);
        setVisible(true);

    }

    private String[] getMenuList(ArrayList<MenuItem> menuItems){
        String[] vec = new String[menuItems.size()+1];
        vec[0] = "None";
        for(int i=0;i< menuItems.size();i++){
            vec[i+1] = menuItems.get(i).getName();
        }
        return vec;
    }

}
