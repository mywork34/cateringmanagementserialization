package BusinessLayer;

import java.util.List;

public class CompositProduct extends MenuItem{
    private List<MenuItem> content;
    private String name;
    private float rating;
    private int noCal;
    private int proteins;
    private int fats;
    private int sodium;
    private float price;

    public CompositProduct(List<MenuItem> content,String name){
        this.content = content;
        this.name = name;
        this.rating = getRating();
        this.noCal = getNoCal();
        this.proteins = getProteins();
        this.fats = getFats();
        this.sodium = getSodium();
        this.price = getPrice();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public float getRating() {
        float rating = 0;
        for(MenuItem b:content){
            rating+= b.getRating();
        }
        rating = rating/content.size();
        return rating;
    }

    @Override
    public int getNoCal() {
        int noCal = 0;
        for(MenuItem b:content){
            noCal += b.getNoCal();
        }
        return noCal;
    }

    @Override
    public int getProteins() {
        int pr = 0;
        for(MenuItem b:content){
            pr += b.getProteins();
        }
        return pr;
    }

    @Override
    public int getFats() {
        int fat = 0;
        for(MenuItem b:content){
            fat += b.getFats();
        }
        return fat;
    }

    @Override
    public int getSodium() {
        int sodium = 0;
        for(MenuItem b:content){
            sodium += b.getSodium();
        }
        return sodium;
    }

    @Override
    public float getPrice() {
        float price = 0;
        for(MenuItem b:content){
            price += b.getPrice();
        }
        price = price*0.9f;
        return price;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public void setRating(float rating) {
        this.rating = rating;
    }

    @Override
    public void setNoCal(int noCal) {
        this.noCal = noCal;
    }

    @Override
    public void setProteins(int proteins) {
        this.proteins = proteins;
    }

    @Override
    public void setFats(int fats) {
        this.fats = fats;
    }

    @Override
    public void setSodium(int sodium) {
        this.sodium = sodium;
    }

    @Override
    public void setPrice(float price) {
        this.price = price;
    }
}
