package PresentationLayer;

import BusinessLayer.DeliveryService;
import DataLayer.AccountValidator;
import Models.Client;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class LoginPanel extends JFrame implements ActionListener
{
    JButton b1,b2;
    JPanel newPanel;
    JLabel userLabel, passLabel;
    final JTextField  textField1, textField2;

    public LoginPanel()
    {

        //create label for username   
        userLabel = new JLabel();
        userLabel.setText("Username");      //set label value for textField1  

        //create text field to get username from the user  
        textField1 = new JTextField(15);    //set length of the text  

        //create label for password  
        passLabel = new JLabel();
        passLabel.setText("Password");      //set label value for textField2  

        //create text field to get password from the user  
        textField2 = new JPasswordField(15);    //set length for the password  

        //create submit button  
        b1 = new JButton("SUBMIT"); //set label to button
        b2 = new JButton("Create Account");
        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AccountValidator av = new AccountValidator();
                if(av.findAccount(textField1.getText(),textField2.getText()) == 1)
                    JOptionPane.showMessageDialog(newPanel,"This account already exists!");
                else{
                    Client c = new Client(textField1.getText(),textField2.getText());
                    av.addAccount(c);
                    JOptionPane.showMessageDialog(newPanel,"Your account was created successfully!");
                }
            }
        });

        //create panel to put form elements  
        newPanel = new JPanel(new GridLayout(3, 1));
        newPanel.add(userLabel);    //set username label to panel  
        newPanel.add(textField1);   //set text field to panel  
        newPanel.add(passLabel);    //set password label to panel  
        newPanel.add(textField2);   //set text field to panel
        newPanel.add(b1);
        newPanel.add(b2);

        //set border to panel   
        add(newPanel,BorderLayout.CENTER);

        //perform action on button click   
        b1.addActionListener(this);     //add action listener to button  
        setTitle("LOGIN FORM");         //set title to the login form  
    }

    //define abstract method actionPerformed() which will be called on button click   
    public void actionPerformed(ActionEvent ae)     //pass action listener as a parameter  
    {
        String userValue = textField1.getText();        //get user entered username from the textField1  
        String passValue = textField2.getText();        //get user entered pasword from the textField2  
        AccountValidator av = new AccountValidator();
        //check whether the credentials are authentic or not  
        if (userValue.equals("admin") && passValue.equals("admin")) {  //if authentic, navigate user to a new page

            //create instance of the NewPage  
            AdminPanel apage = new AdminPanel();
            apage.setLocation(200,200);
            apage.setVisible(true);

            //create a welcome label and set it to the new page

        }
        else if(av.findAccount(userValue,passValue) == 1){
            DeliveryService ds = new DeliveryService();
            ds.setCurrentClient(av.getClient(userValue,passValue));
            ClientPanel cpage = new ClientPanel();
            cpage.setLocation(950,200);
            cpage.setVisible(true);
        }
        else{
            //show error message  
            JOptionPane.showMessageDialog(newPanel,"You don't have an account!");
        }
    }

}
