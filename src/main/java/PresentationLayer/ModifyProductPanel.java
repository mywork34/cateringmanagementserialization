package PresentationLayer;

import BusinessLayer.BaseProduct;
import BusinessLayer.DeliveryService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ModifyProductPanel extends JFrame{
    private JLabel indexL;
    private JTextField indexT;
    private JPanel info;
    private JLabel nameL;
    private JTextField nameT;
    private JLabel ratingL;
    private JTextField ratingT;
    private JLabel calL;
    private JTextField calT;
    private JLabel proL;
    private JTextField proT;
    private JLabel fatL;
    private JTextField fatT;
    private JLabel sodL;
    private JTextField sodT;
    private JLabel priceL;
    private JTextField priceT;
    private JButton submit;

    public ModifyProductPanel(){
        info = new JPanel(new GridLayout(9,2));

        indexL = new JLabel("Index: ");
        indexT = new JTextField();
        info.add(indexL);
        info.add(indexT);

        nameL = new JLabel("Name: ");
        nameT = new JTextField();
        info.add(nameL);
        info.add(nameT);

        ratingL = new JLabel("Rating: ");
        ratingT = new JTextField();
        info.add(ratingL);
        info.add(ratingT);

        calL = new JLabel("No. Calories: ");
        calT = new JTextField();
        info.add(calL);
        info.add(calT);

        proL = new JLabel("No. Proteins: ");
        proT = new JTextField();
        info.add(proL);
        info.add(proT);

        fatL = new JLabel("No. Fats: ");
        fatT = new JTextField();
        info.add(fatL);
        info.add(fatT);

        sodL = new JLabel("No. sodium: ");
        sodT = new JTextField();
        info.add(sodL);
        info.add(sodT);

        priceL = new JLabel("Price: ");
        priceT = new JTextField();
        info.add(priceL);
        info.add(priceT);

        submit = new JButton("SUBMIT");
        info.add(submit);

        submit.addActionListener(new ModifyProductPanel.submitActionListener());

        setTitle("Modify Product");
        setSize(250,200);
        add(info);
        setVisible(true);
    }

    class submitActionListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            DeliveryService deliveryService = new DeliveryService();
            deliveryService.modifyItem(Integer.parseInt(getIndexT()),
                                        getNameT(),
                                        Float.parseFloat(getRatingT()),
                                        Integer.parseInt(getCalT()),
                                        Integer.parseInt(getProT()),
                                        Integer.parseInt(getFatT()),
                                        Integer.parseInt(getSodT()),
                                        Float.parseFloat(getPriceT()));
            JOptionPane.showMessageDialog(info,"The product with index "+ getIndexT() + " was modified!");
            dispose();
        }
    }

    public String getIndexT(){
        return indexT.getText();
    }

    public String getNameT() {
        return nameT.getText();
    }

    public String getRatingT() {
        return ratingT.getText();
    }

    public String getCalT() {
        return calT.getText();
    }

    public String getProT() {
        return proT.getText();
    }

    public String getFatT() {
        return fatT.getText();
    }

    public String getSodT() {
        return sodT.getText();
    }

    public String getPriceT() {
        return priceT.getText();
    }
}
