package DataLayer;

import BusinessLayer.DeliveryService;
import BusinessLayer.MenuItem;
import BusinessLayer.Order;
import Models.Client;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

public class Serializator implements Serializable {
    private static String productsFile = "menu.txt";
    private static String accountsFile = "accounts.txt";
    private static String ordersFile = "orders.txt";

    public static void serializeProducts(){
        try {
            FileOutputStream file = new FileOutputStream(productsFile);
            ObjectOutputStream out = new ObjectOutputStream(file);
            ArrayList<MenuItem> menuList = DeliveryService.getMenuItems();
            for(MenuItem i:menuList)
                out.writeObject(i);
            out.close();
            file.close();
        }catch (Exception ex){
            System.out.println("Failed to serialize");
            //ex.printStackTrace();
        }
    }

    public static void deserializeProducts(){
        try {
            FileInputStream file = new FileInputStream(productsFile);
            ObjectInputStream in = new ObjectInputStream(file);
            DeliveryService deliveryService = new DeliveryService();
            MenuItem item = (MenuItem) in.readObject();
            while(item!=null){
                deliveryService.addMenuItem(item);
                item = (MenuItem) in.readObject();
            }
            in.close();
            file.close();
        }catch (Exception ex){
            System.out.println("Deserialized complete![Products]");
            //ex.printStackTrace();
        }
    }

    public static void serializeAccounts(){
        try {
            FileOutputStream file = new FileOutputStream(accountsFile);
            ObjectOutputStream out = new ObjectOutputStream(file);
            ArrayList<Client> acc = AccountValidator.getAccounts();
            for(Client c: acc)
                out.writeObject(c);
            out.close();
            file.close();
        } catch (Exception ex) {
            System.out.println("Serialization complete!");
        }
    }

    public static void deserializeAccounts(){
        try{
            FileInputStream file = new FileInputStream(accountsFile);
            ObjectInputStream in = new ObjectInputStream(file);
            AccountValidator av = new AccountValidator();
            Client acc = (Client) in.readObject();
            while(acc != null){
                av.addAccount(acc);
                acc = (Client) in.readObject();
            }

            in.close();
            file.close();
        }catch (Exception ex){
            System.out.println("Deserilization complete![Accounts]");
        }
    }

    public static void serializeOrders(){
        try {
            FileOutputStream file = new FileOutputStream(ordersFile);
            ObjectOutputStream out = new ObjectOutputStream(file);
            out.writeObject(DeliveryService.getOrders());
            out.close();
            file.close();
        } catch (Exception ex) {
            System.out.println("Serialization complete!");
        }
    }

    public static void deserializeOrders(){
        try{
            FileInputStream file = new FileInputStream(ordersFile);
            ObjectInputStream in = new ObjectInputStream(file);
            HashMap<Order, ArrayList<MenuItem>> orders =(HashMap<Order, ArrayList<MenuItem>>) in.readObject();
            int cont = 0;
            for(Order order: orders.keySet())
                cont++;
            DeliveryService.setOrders(orders);
            Order.setOrderCount(cont);
            in.close();
            file.close();
        }catch (Exception ex){
            System.out.println("Deserilization complete![Orders]");
        }
    }
}
