package PresentationLayer;

import BusinessLayer.DeliveryService;
import BusinessLayer.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class ClientPanel extends JFrame {
    private JPanel clientFunctions;
    private JButton viewMenuButton = new JButton("ViewMenu");
    private JButton searchButton = new JButton("Search");
    private JButton orderButtom = new JButton("PlaceOrder");

    public ClientPanel(){
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("CLIENT");
        setSize(400,200);

        clientFunctions = new JPanel(new GridLayout(3,1));
        clientFunctions.add(viewMenuButton);
        clientFunctions.add(searchButton);
        clientFunctions.add(orderButtom);

        viewMenuButton.addActionListener(new viewMenuActionListener());
        searchButton.addActionListener(new searchActionListener());
        orderButtom.addActionListener(new orderActionListener());

        add(clientFunctions);

    }

    class viewMenuActionListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            ClientViewMenu cm = new ClientViewMenu();
        }
    }

    class searchActionListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            ClientSearchMenu cm = new ClientSearchMenu();
        }
    }
    class orderActionListener implements ActionListener{
        public void actionPerformed(ActionEvent e){
            DeliveryService ds = new DeliveryService();
            ArrayList<MenuItem> listItems = ds.getMenuItems();
            String[] menuList = getMenuList(listItems);
            ClientPlaceOrder co = new ClientPlaceOrder(menuList);
        }
    }


    class ClientViewMenu extends JFrame{
        private JPanel menuList;
        private JLabel menuLabel = new JLabel("The menu: ");
        private JComboBox cbMenu;

        public ClientViewMenu(){
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            setTitle("OUR MENU");
            setSize(400,80);
            setLocation(950,100);

            DeliveryService deliveryService = new DeliveryService();

            String[] mList = getMenuList(deliveryService.getMenuItems());
            menuList = new JPanel(new GridLayout(1,2,1,1));
            cbMenu = new JComboBox(mList);

            menuList.add(menuLabel);
            menuList.add(cbMenu);

            add(menuList);
            setVisible(true);
        }

    }

    class ClientSearchMenu extends JFrame{
        private JPanel searchPanel;
        private JLabel searchL = new JLabel("Search criteria:");
        private JLabel searchL2 = new JLabel("Search data:");
        private JComboBox criteriaBox;
        private JComboBox resultBox;
        private String cbtext;
        private JTextField searchT;
        private JButton searchButton = new JButton("SEARCH");

        public ClientSearchMenu(){
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            setTitle("SearchMenu");
            setSize(400,150);
            setLocation(950,400);

            String[] criterias = new String[]{"Name","Rating","Calories","Price"};
            criteriaBox = new JComboBox(criterias);
            searchT = new JTextField();

            searchPanel = new JPanel(new GridLayout(3,2,1,1));

            criteriaBox.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setCBText(criteriaBox.getSelectedItem().toString());
                }
            });

            searchButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DeliveryService deliveryService = new DeliveryService();
                    List<MenuItem> result = deliveryService.searchProducts(cbtext,searchT.getText());
                    String[] menuList = getMenuList((ArrayList<MenuItem>) result);
                    ShowResults sr = new ShowResults(menuList);
                    System.out.println(cbtext+" "+ searchT.getText());
                }
            });

            searchPanel.add(searchL);
            searchPanel.add(criteriaBox);
            searchPanel.add(searchL2);
            searchPanel.add(searchT);
            searchPanel.add(searchButton);


            add(searchPanel);
            setVisible(true);
        }

        public void setCBText(String s){
            this.cbtext = s;
        }
    }

    class ShowResults extends JFrame{
        private JPanel resPanel;
        private JLabel resL = new JLabel("Search results:");
        private JComboBox resCB;

        public ShowResults(String[] menuList){
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            setTitle("SearchResults");
            setSize(400,50);
            setLocation(950,560);

            resPanel = new JPanel(new GridLayout(1,2,1,1));
            resCB = new JComboBox(menuList);

            resPanel.add(resL);
            resPanel.add(resCB);
            add(resPanel);
            setVisible(true);
        }
    }

    class ClientPlaceOrder extends JFrame{
        private JPanel orderPanel;
        private JButton addProductButton;
        private JButton submitButton;
        private JLabel selectL = new JLabel("Select items:");
        private JComboBox cbMenu;
        private String selectedProduct;
        private String finalProductName;
        private ArrayList<MenuItem> products;

        public ClientPlaceOrder(String[] menuList){
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            setTitle("PlaceOrder");
            setSize(400,50);
            setLocation(950,410);
            products = new ArrayList<MenuItem>();

            orderPanel = new JPanel(new GridLayout(2,2,1,1));
            addProductButton = new JButton("AddProduct");
            submitButton = new JButton("SUBMIT");
            cbMenu = new JComboBox(menuList);

            cbMenu.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    setCBText(cbMenu.getSelectedItem().toString());
                }
            });

            addProductButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DeliveryService deliveryService = new DeliveryService();
                    setFinalProductName(getCBText());
                    MenuItem item = deliveryService.getItemAfterName(getFinalProductName());
                    getProducts().add(item);
                    JOptionPane.showMessageDialog(orderPanel,"Item name "+item.getName()+" rating "+item.getRating()+"was added!");
                }
            });

            submitButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    DeliveryService deliveryService = new DeliveryService();
                    deliveryService.placeOrder(getProducts());
                    JOptionPane.showMessageDialog(orderPanel,"Your order was submitted!");
                    dispose();
                }
            });

            orderPanel.add(selectL);
            orderPanel.add(cbMenu);
            orderPanel.add(addProductButton);
            orderPanel.add(submitButton);

            add(orderPanel);
            setVisible(true);
        }
        public void setCBText(String s){
            this.selectedProduct = s;
        }
        public String getCBText(){return selectedProduct;}
        public void setFinalProductName(String s){
            this.finalProductName = s;
        }
        public String getFinalProductName(){return finalProductName;}
        public ArrayList<MenuItem> getProducts(){return products;}
    }

    private String[] getMenuList(ArrayList<MenuItem> menuItems){
        String[] vec = new String[menuItems.size()+1];
        vec[0] = menuItems.size()+"";
        for(int i=0;i< menuItems.size();i++){
            vec[i+1] = menuItems.get(i).getName();
        }
        return vec;
    }

}
