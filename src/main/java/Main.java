import javax.swing.*;

import DataLayer.Serializator;
import PresentationLayer.LoginPanel;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;


public class Main {
    public static void main(String[] arg)
    {
        Serializator.deserializeProducts();
        Serializator.deserializeAccounts();
        Serializator.deserializeOrders();

        LoginPanel form = new LoginPanel();
        form.setSize(300,100);
        form.setLocation(640,200);
        form.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        form.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                Serializator.serializeProducts();
                Serializator.serializeAccounts();
                Serializator.serializeOrders();
                System.exit(0);
            }
        });
        form.setVisible(true);



    }
}
