package BusinessLayer;

import Models.Client;

import java.awt.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DeliveryService {
    private static ArrayList<BaseProduct> baseProducts= new ArrayList<BaseProduct>();
    private static HashSet<String> names = new HashSet<String>();
    private static ArrayList<MenuItem> menuItems = new ArrayList<MenuItem>();
    private static HashMap<Order, ArrayList<MenuItem>> orders = new HashMap<Order, ArrayList<MenuItem>>();
    private static Client currentClient;

    public void getProducts(){
        String fName = "products.csv";
        baseProducts.clear();
        names.clear();
        try (Stream<String> rows = Files.lines(Paths.get((fName)))) {
            baseProducts = (ArrayList<BaseProduct>) rows.skip(1).map(mapId).filter(s -> s!=null).collect(Collectors.toCollection(ArrayList::new));
        }catch (Exception ex){
            ex.printStackTrace();
        }
        menuItems.clear();
        for(BaseProduct product : baseProducts){
            menuItems.add(product);
        }
    }

    public Function<String,BaseProduct> mapId = row -> {
        String[] coll = row.split(",");
        if(names.contains(coll[0])){
            return null;
        }
        else{
            names.add(coll[0]);
            String n = coll[0].trim();
            float rating = Float.parseFloat(coll[1].trim());
            int noCal = Integer.parseInt(coll[2].trim());
            int pro = Integer.parseInt(coll[3].trim());
            int fat = Integer.parseInt(coll[4].trim());
            int sodium = Integer.parseInt(coll[5].trim());
            float price = Float.parseFloat(coll[6].trim());
            BaseProduct item = new BaseProduct(n,rating,noCal,pro,fat,sodium,price);
            return item;
        }
    };

    public void addBaseProduct(BaseProduct item){
        baseProducts.add(item);
        menuItems.add(item);
    }
    public void removeItem(int index){
        menuItems.remove(index);
    }

    public void modifyItem(int index,String name,float rating,int noCal,int proteins,int fats,int sodium,float price){
        MenuItem item = menuItems.get(index);
        item.setName(name);
        item.setRating(rating);
        item.setNoCal(noCal);
        item.setProteins(proteins);
        item.setFats(fats);
        item.setSodium(sodium);
        item.setPrice(price);
    }

    public void addCompositeProduct(){

    }

    public MenuItem getItemAfterName(String name){
        for(MenuItem it : menuItems){
            if(it.getName().equals(name)){
                return it;
            }
        }
        return null;
    }

    public  List<MenuItem>  searchProducts(String criteria,String value){
        List<MenuItem> result = new LinkedList<MenuItem>();

        if(criteria.equals("Name")) {
            result = menuItems.stream().filter(s -> s.getName().toLowerCase(Locale.ROOT).contains(value) || s.getName().toLowerCase(Locale.ROOT).equals(value))
                    .collect(Collectors.toList());
        }
        else if(criteria.equals("Rating")){
            result = menuItems.stream().filter(s -> s.getRating()==Integer.parseInt(value))
                    .collect(Collectors.toList());
        }
        else if(criteria.equals("Calories")){
            result = menuItems.stream().filter(s -> s.getNoCal()==Integer.parseInt(value))
                    .collect(Collectors.toList());
        }
        else if(criteria.equals("Price")){
            result = menuItems.stream().filter(s -> s.getPrice()==Integer.parseInt(value))
                    .collect(Collectors.toList());
        }
        return result;
    }

    public ArrayList<BaseProduct> getBaseProducts() {
        return baseProducts;
    }

    public static ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }

    public HashSet<String> getNames() {
        return names;
    }

    public static HashMap<Order, ArrayList<MenuItem>> getOrders(){ return orders;}

    public static void setOrders(HashMap<Order, ArrayList<MenuItem>> o){orders = o;}

    public void listMenuItems(){
        System.out.println("There are " + menuItems.size() +" items in our menu!");
        for(MenuItem i:menuItems){
            System.out.println(i);
        }
    }

    public void placeOrder(ArrayList<MenuItem> orderList){
        float cost = 0;
        for(MenuItem orderProduct : orderList)
            cost += orderProduct.getPrice();
        Random rand = new Random();
        int clientID = currentClient.getClientId();
        Order order = new Order(clientID, LocalDateTime.now(), cost);
        LinkedList<MenuItem> list = new LinkedList<>();
        orders.put(order, orderList);
    }

    public void addMenuItem(MenuItem item){ menuItems.add(item);}

    public void setCurrentClient(Client currentClient) {
        this.currentClient = currentClient;
    }
}
