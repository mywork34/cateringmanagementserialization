package DataLayer;

import Models.Client;

import java.util.ArrayList;

public class AccountValidator {
    private static ArrayList<Client> accounts = new ArrayList<Client>();

    public void addAccount(Client client){
        accounts.add(client);
    }
    public int findAccount(String username,String password){
        for(Client c : accounts){
            if(c.getPassword().equals(password) && c.getUsername().equals(username)){
                return 1;
            }
        }
        return 0;
    }

    public Client getClient(String username,String password){
        for(Client c:accounts){
            if(c.getPassword().equals(password) && c.getUsername().equals(username)){
                return c;
            }
        }
        return null;
    }

    public static ArrayList<Client> getAccounts(){return accounts;};
}
